SHELL := /bin/bash
bkpath:=$$HOME/vim-bk-$(shell date +"%Y%m%d-%H-%M-%S")

here  :=$(shell pwd)


all: clean link
	@echo installing all plugins ...
	vim +PlugInstall +qall
	- dos2unix ${here}/plugged/vim-transpose/plugin/transpose.vim



vimplug: autoload/plug.vim
	@echo install vim-plug done!

autoload/plug.vim:
	curl -fLo autoload/plug.vim --create-dirs \
		    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

backup: 
	@echo backup your old vim setup into ${bkpath} ...
	- mkdir -p ${bkpath}
	- mv $$HOME/.vim* ${bkpath}

link: vimplug backup
	@echo link ...
	ln -s ${here} $$HOME/.vim
	ln -s ${here}/vimrc $$HOME/.vimrc

clean:
	rm -rf plugged
	rm -f autoload/plug.vim
