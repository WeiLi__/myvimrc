set nocompatible
filetype off

call plug#begin('~/.vim/plugged')

Plug 'ervandew/supertab'
Plug 'vim-scripts/taglist.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-scripts/winmanager'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Rykka/riv.vim'
Plug 'tpope/vim-fugitive'
Plug 'tmux-plugins/vim-tmux'
Plug 'vim-scripts/VisIncr'
Plug 'salsifis/vim-transpose'
Plug 'godlygeek/tabular'

call plug#end()

filetype indent on
filetype plugin on
let g:is_bash=1

"remember last modify place
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

set splitright
"autocmd FileType qf wincmd L

set mouse=a
set t_Co=256

let mapleader = ","

"always show status line
set laststatus=2

"search hilight
map <leader>hs :set hlsearch!<cr>
"underline
map <leader>ul :set cursorline!<cr>

"quick navgate between windows
map <C-Left>   <C-W>h
map <C-Right>  <C-W>l
map <C-Up>     <C-W>k
map <C-Down>   <C-W>j

"set line number
map <leader>nn :set nu!<cr>
map <leader>rn :set rnu!<cr>
"au InsertEnter * :set nu
au InsertLeave * :set rnu
au FocusGained * :set rnu
au CursorMoved * :set rnu
"au CursorHold * :set nu

"substitude the select thing
vmap ;y y:%s/<C-R>"/<C-R>"/g

"swatch the <shit-arrow> function in different envirement 
au WinEnter * :call ToggleSift()
au FileType nerdtree :call ToggleSift()
au FileType qf :call ToggleSift()
"--------------------------------------------------------------------------------------

function! ToggleSift()
        "set wrap
        map <S-Left> :bn<CR>
        map <S-Right> :bp<CR>
        let l:fname = expand('%:t') 
        echo l:fname
        if (( l:fname == '-MiniBufExplorer-' )  || (&ft == 'nerdtree') || (l:fname == '[File List]'))
                "echo 'make sift not work'
                unmap <S-Left>
                unmap <S-Right>
                return
        endif
        if ( &ft == 'qf' )

                map <S-Left> :colder<CR>
                map <S-Right> :cnewer<CR>

        endif
        if ( l:fname == '-MiniBufExplorer-' )
                set nowrap
        endif
endfunction

call ToggleSift()

function! CleanClose(tosave)
        if (a:tosave == 1)
                w!
        endif
        let todelbufNr = bufnr("%")
        let newbufNr = bufnr("#")
        if ((newbufNr != -1) && (newbufNr != todelbufNr) && buflisted(newbufNr))
                exe "b!".newbufNr
        else
                bnext
        endif
        if (bufnr("%") == todelbufNr)
                new
        endif
        exe "bd!".todelbufNr
endfunction

map fc <Esc>:call CleanClose(1)<CR>
map fq <Esc>:call CleanClose(0)<CR>

"------------Airline-------------------------------------------------------------------
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" unicode symbols
let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
"let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.space = "\ua0"

"let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='simple'        
"--------------------------------------------------------------------------------------

let g:winManagerWindowLayout='FileExplorer,TagList'
"let g:winManagerWindowLayout='TagList'
let g:winmanagerWidth=38    
nmap <silent> <F2> :WMToggle<cr>     

"NERDTree settings
let g:NERDTreeWinSize=35
map  <silent> <F4> :NERDTreeTabsToggle<cr>
"only nerdtree left close it too
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
"no file open with vim, turn on nerdtree default
"let g:nerdtree_tabs_open_on_console_startup=1
let g:nerdtree_tabs_autofind=1

"minibufexpl settings
let g:miniBufExplMapWindowNavArrows = 1
"let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1
let g:miniBufExplorerMoreThanOne=0

set nowrap
set autochdir 

"Fast reloading of the .vimrc
map <silent> <leader>ss :source ~/.vimrc<cr>
"Fast editing of .vimrc
map <silent> <leader>ee :e ~/.vimrc <cr>
"When .vimrc is edited, reload it
autocmd! bufwritepost .vimrc source ~/.vimrc

"map for Tab page
nmap <silent> [t :tabn<CR>
nmap <silent> ]t :tabp<CR>
nmap t% :tabedit %<CR>
nmap tc :tabclose<CR>

"set font of gvim: note! [yourfont]\ [size] to make sure the interval of
"letter not too big
if has ("gui_running")
    colorscheme graywh0
    set guifont=Monospace\ 12
elseif &diff  
    colorscheme peaksea  
else
    colorscheme graywh
    set guifont=Consolas\ 12
    "set guifont=Source\ Code\ Pro\ for\ Powerline\ 13
endif

"set fold
map <leader>fdi :set fdm=indent<cr> 
map <leader>fdm :set fdm=marker<cr> 

"Fortran语言设置[1~6]
"1. 允许使用tab
let fortran_have_tabs=0
"2. 允许fortran折叠
let fortran_fold=1
"3. 总是使用自由格式
"let fortran_free_source=1
"4. 语法高亮更精确
let fortran_more_precise=1
"5. 文件的语法折叠
let fortran_fold_conditionals=1
let fortran_fold_multilinecomments=1
"6. do 循环缩进
"let fortran_do_enddo=1

set list
set listchars=tab:\`\ 
"set listchars=tab:▸\ ,eol:¬

"set tab size
set smartindent
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab

"NERD_commenter  Ctrl+h
map <c-h> ,c<space>

"resize window
map ;h <c-W><10
map ;l <c-W>>10
map ;r <c-W>=

"SuperTab setings
let g:SuperTabRetainCompletionType=2
"let g:SuperTabDefaultCompletionType="<C-X><C-O>"
"use space choose the complete popup
inoremap <expr> <Space> pumvisible() ? "\<C-y>" : " "

"Grep.vim setting 
nnoremap <silent> <F3> :Grep<CR>
let g:tex_flavor='latex'
set grepprg=grep\ -nH\ $*

"open vim help of the thing on the cursor
map hp "zyw:exe "h ".@z.""<CR>

"set riv
let g:riv_highlight_code='lua,python,cpp,javascript,vim,sh,fortran,awk,tex'

au BufNewFile,BufRead *.tex set wrap

set keywordprg=sdcv

set makeprg=atdtool\ %

syntax enable
syntax on
